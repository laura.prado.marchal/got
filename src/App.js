import logo from './logo.svg';
import './App.css';
import { BrowserRouter as Router } from 'react-router-dom';
import Routes from './core/Routes/Routes';

function App() {
  return (
    <>
      <h1>Hola</h1>
      <Router>
        <Routes></Routes>
      </Router>
    </>
  );
}

export default App;
