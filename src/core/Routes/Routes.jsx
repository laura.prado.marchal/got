import React from 'react';
import { Route, Switch } from 'react-router-dom';
import CharactersPage from '../../pages/CharactersPage/CharactersPage';
import CharacterDetail from '../../pages/CharactersPage/CharacterDetail/CharacterDetail';
import HousesPage from '../../pages/HousesPage/HousesPage';



export default function Routes() {
    return (
        <Switch>
            <Route path="/characters/:idCharacter">
                <CharacterDetail></CharacterDetail>
            </Route>
            <Route path="/characters">
                <CharactersPage></CharactersPage>
            </Route>
            <Route path="/houses">
                <HousesPage></HousesPage>
            </Route>
        </Switch>
    )
}