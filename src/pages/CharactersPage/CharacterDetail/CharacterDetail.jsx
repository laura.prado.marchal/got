import React, { useEffect } from 'react';

import {
    BrowserRouter as Router,
    Switch,
    Route,
    useParams,
    useRouteMatch,
    useLocation,
    useHistory
  } from "react-router-dom";



export default function CharacterDetail(props) {

    const { idCharacter } = useParams();

    return (
        <div>
            {/* Preguntar porque map sale como undefined cuando esta puesto el array vacio */}
            {props.characters.map((character, index) => {
                if (character.id === idCharacter) {
                    return <div key={index}>{character.name}</div>
                }
            })}
        </div>
    )

}

