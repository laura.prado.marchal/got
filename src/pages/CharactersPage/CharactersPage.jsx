import React, { useEffect, useState } from 'react';
import axios from 'axios'
import CharactersGallery from './ChractersGallery';
import CharacterDetail from './CharacterDetail/CharacterDetail';



export default function CharactersPage() {

    const [characters, setCharacters] = useState([]);
    useEffect(() => {
        axios.get('https://api.got.show/api/show/characters').then(res => {
            //console.log(res);
            setCharacters(res.data)
            console.log(characters)
        })
    }, [])

    return (
        <>
            <CharactersGallery characters={characters}></CharactersGallery>
            <CharacterDetail characters={characters}></CharacterDetail>
        </>
    )


}