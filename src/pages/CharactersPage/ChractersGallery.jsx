import React, { useEffect, useState } from 'react';
import axios from 'axios'
import { Link } from 'react-router-dom';



export default function CharactersGallery(props) {

    

    return (
        <div>
            {props.characters.map((character,index) => <li key={index}><Link to={'characters/' + character.id}>{character.name}</Link></li>)}
        </div>
    )


}