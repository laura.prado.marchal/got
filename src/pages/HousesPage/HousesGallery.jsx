import React, { useEffect, useState } from 'react';
import axios from 'axios'
import { Link } from 'react-router-dom';



export default function HousesGallery(props) {

    

    return (
        <div>
            {props.houses.map((house,index) => <li key={index}><Link to={'houses/' + house.id}>{house.name}</Link></li>)}
        </div>
    )


}