import React, { useEffect, useState } from 'react';
import axios from 'axios';
import HousesGallery from './HousesGallery';





export default function HousesPage() {

    const [houses, setHouses] = useState([]);
    useEffect(() => {
        axios.get('https://api.got.show/api/show/houses').then(res => {
            //console.log(res);
            setHouses(res.data)
            console.log(houses)
        })
    }, [])

    return (
       
            <HousesGallery houses={houses}></HousesGallery>
            
        
    )


}